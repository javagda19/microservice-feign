package com.java19.calculatorservice.service;

import org.springframework.stereotype.Service;

@Service
public class CalcService {

    public double add(double p1, double p2) {
        return p1 + p2;
    }

    public double sub(double p1, double p2) {
        return p1 - p2;
    }
}
