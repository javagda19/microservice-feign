package com.java19.calculatorservice.controller;

import com.java19.calculatorservice.model.dto.CalculationDto;
import com.java19.calculatorservice.service.CalcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/calc")
public class CalcController {

    @Autowired
    private CalcService calcService;

    @PostMapping("/calculateSum")
    public double getSumOf(@RequestBody CalculationDto calculationDto){
        return calcService.add(calculationDto.getP1(), calculationDto.getP2());
    }

    @GetMapping("/calculateSumGet")
    public double getSumOf(@RequestParam(name = "p1") Double p1,
                           @RequestParam(name = "p2") Double p2){
        return calcService.add(p1, p2);
    }
}
