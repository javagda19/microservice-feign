package com.java19.frontcalculatora;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class FrontcalculatoraApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrontcalculatoraApplication.class, args);
    }

}
