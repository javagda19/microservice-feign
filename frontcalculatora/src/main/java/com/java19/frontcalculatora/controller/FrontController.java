package com.java19.frontcalculatora.controller;

import com.java19.frontcalculatora.model.dto.CalculationDto;
import com.java19.frontcalculatora.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class FrontController {

    @Autowired
    private CalculatorService calculatorService;

    @GetMapping("/calc")
    public String calc(Model model){
        model.addAttribute("dto", new CalculationDto());
        return "calculatorInterface";
    }

    @PostMapping("/calc")
    public String calculate(Model model, CalculationDto dto){
        double result = calculatorService.calculate(dto);

        model.addAttribute("result", result);

        return "resultView";
    }
}
