package com.java19.frontcalculatora.api;

import com.java19.frontcalculatora.model.dto.CalculationDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("j19calledservice")
public interface CalledAPIClient {

    @PostMapping("/calc/calculateSum")
    public double getSumOf(@RequestBody CalculationDto calculationDto);

    @GetMapping("/calc/calculateSumGet")
    public double getSumOf(@RequestParam(name = "p1") Double p1,
                           @RequestParam(name = "p2") Double p2);
}
