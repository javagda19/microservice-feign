package com.java19.frontcalculatora.service;

import com.java19.frontcalculatora.api.CalledAPIClient;
import com.java19.frontcalculatora.model.dto.CalculationDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CalculatorService {
    @Autowired
    private CalledAPIClient client;

    public double calculate(CalculationDto dto){
        log.info("Calling.");
        double res = client.getSumOf(dto);
        log.info("Result: " + res);
        return res;
    }

}
